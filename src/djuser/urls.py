"""
base url page.
"""
from django.urls import path
from django.contrib import admin
from accounts.views import (
    login_view,
    register_view,
    logout_view
)
from .views import home

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('accounts/login/', login_view),
    path('accounts/register/', register_view),
    path('accounts/logout/', logout_view)
]
