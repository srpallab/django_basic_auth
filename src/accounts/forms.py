from django import forms
from django.contrib.auth import authenticate, get_user_model

User = get_user_model()


class UserLoginForm(forms.Form):
    """Documentation for UserLoginForm"""
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if not user:
                raise forms.ValidationError('This user is not exist!')
            if not user.check_password(password):
                raise forms.ValidationError('Password is incorrect!')
            if not user.is_active:
                raise forms.ValidationError('This user is not active')
        return super(UserLoginForm, self).clean(*args, **kwargs)


class UserRegisterForm(forms.ModelForm):
    email = forms.EmailField(label='Email Address')
    email2 = forms.EmailField(label='Confirm Email')
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'email2',
            'password'
        ]

        def clean(self, *args, **kwargs):
            email = self.clean_data.get('email')
            email2 = self.clean_data.get('email2')
            if email != email2:
                raise forms.ValidationError("Email must be same!")
            email_qs = User.objects.filter(email=email)
            if email_qs.exist():
                raise forms.ValidationError(
                    "This email account already exist"
                )
            return super(UserRegisterForm, self).clean(*args, **kwargs)
